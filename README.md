# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This explains the working of sports matches data utility
* 0.1

### How do I get set up? ###

* Checkout the project
* Run: 
      mvn clean install
      mvn spring-boot:run
* Dependencies
* Database configuration
  Uses an internal H2 database
* How to run tests
  mvn clean test
* Deployment instructions
  Not applicable

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### APIs ###

1. Fetching from Source A

GET http://localhost:8080/sportsdata/sourceA
Response:
{
    "matchesList": [
        {
            "id": "804.745969526437",
            "league": "PremierLeague",
            "home": "Arsenal",
            "away": "Chelsea",
            "date": "24 September 2016",
            "home_score": "3",
            "away_score": "0"
        },
        {
            "id": "8707.262688153425",
            "league": "PremierLeague",
            "home": "Arsenal",
            "away": "Everton",
            "date": "21 May 2017",
            "home_score": "1",
            "away_score": "3"
        }
    ]
}

2. Performing a ETL from Source A to Source B

POST http://localhost:8080/sportsdata/performETL
Response:
{
    "status": "Success",
    "statusText": "ETL complete from Source A to Source B"
} 



3. Searching elements in Source B

GET http://localhost:8080/sportsdata/matches?home=Arsenal
Response:
{
    "matchesList": [
        {
            "id": "1",
            "league": "PremierLeague",
            "home": "Arsenal",
            "away": "Chelsea",
            "date": "24/09/2016",
            "home_score": "3",
            "away_score": "0"
        },
        {
            "id": "2",
            "league": "PremierLeague",
            "home": "Arsenal",
            "away": "Everton",
            "date": "21/05/2017",
            "home_score": "1",
            "away_score": "3"
        }
    ]
}

### Who do I talk to? ###

* Repo owner or admin
  nelson.dalmeida22@gmail.com
  +61 426725685